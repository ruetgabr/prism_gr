# Chelsea Manning

_Chelsea Elizabeth Manning_, née _Bradley Edward Manning_ le 17 décembre 1987 à Crescent (Oklahoma), est une ancienne analyste militaire de l'Armée des États-Unis de nationalité américano-britannique qui a été condamnée et incarcérée pour trahison.