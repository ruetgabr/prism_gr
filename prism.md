PRISM (également appelé US-984XN1), est un programme américain de surveillance électronique par la collecte de renseignements à partir d'Internet et d'autres fournisseurs de services électroniques2,3,4,note 1. Ce programme classé, relevant de la National Security Agency (NSA), prévoit le ciblage de personnes vivant hors des États-Unis10. PRISM est supervisé par la United States Foreign Intelligence Surveillance Court (FISC) conformément au FISA Amendments Act of 2008 (FISA)11. 



En France, le sénat entérine la Loi de programmation militaire (LPM) le 18 décembre 2013, laquelle autorise la police, la gendarmerie, ainsi que les services habilités des ministères de la Défense, de l’Économie et du Budget à surveiller les citoyens sur les réseaux informatiques sans l'autorisation d'un juge44,45,46.

The Guardian a révélé l’existence d’un autre outil en plus de PRISM, XKeyscore, à la finalité différente puisqu'il permet d'effectuer des croisements sur la base notamment des données de PRISM. 
